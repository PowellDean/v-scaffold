# V-Scaffold

A small program, written in V, to scaffold new project directory layouts.
Ironically, it currently only supports the creation of new Python projects.

# Purpose

V-Scaffold was written because creating a new programming project can be a bit
annoying. There are many options for how to set up a project's directory
structure, and many differing alternatives can be rather confusing. Not only
that, but Python especially prefers the use of virtual environments, which
then encourage the use of dependency managers like Poetry or Pipenv, each of
which in turn have their own commands for setting things up.

V-Scaffold takes the drudgery out of creating a new project by doing all the
awful setup for you, and it always sets up projects with the identical
structure and dependencies installed.

## Python Projects

Inside the new project, the latest version of the Sphinx documentation
generator will be installed as a dependency, as will the free version of
_safety_, a Python dependency checker. And to make unit testing easier,
the latest version of _pytest_ is also installed. All dependencies are
installed in a new virtual environment sandbox.

V-scaffold will create the following new project structure:

```
new_project/
├─ docs/
│  ├─ build/
│  ├─ make.bat
│  ├─ Makefile
│  ├─ source/
│  │  ├─ conf.py
│  │  ├─ index.rst
│  │  ├─ _static/
│  │  ├─ _templates/
├─ poetry.lock
├─ pyproject.toml
├─ pytest.ini
├─ README.rst
├─ src
│  ├─ new_project/
│  │  ├─ __init__.py
├─ TestResults
│  ├─ report.xml
├─ tests
│  ├─ __init__.py
│  ├─ test_new_project.py
```

Some of this structure is created by V-Scaffold itself, some of it is created
by the Poetry dependency manager, and some of it is created by the Sphinx
documentation generator.

V-Scaffold will also run initial Sphinx setup in the new virtual environment,
and it will perform an initial run of pytest, just to make sure everything is
done for you and ready for you to start coding.

# Pre-Requisites

Currently, the Python dependency management tool _Poetry_ is a requirement.
At some point, I may also allow _pipenv_ to be used. Once other languages are
supported, Poetry/Pipenv will be necessary only for creating new Python
projects.

# Usage

```
scaffold OPTIONS <name>

OPTIONS

-a, --author <v>         Author
-h, --help               print help and exit
-l, --language <python>  The language this is written in


ARGUMENTS

name                     The name of the project to create
```

## Examples

Create a new python project named myprj:

```
scaffold --language python myprj
```

Another way to create a new python project named myprj

```
scaffold myprj
```

# License

Apache License, Version 2.0
