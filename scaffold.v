//   Copyright 2022 Dean Powell <PowellDean@gmail.com>
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import powelldean.getopts.getopts
import os

struct Config {
    mut:
        prj_name string
        language string
        author string
}

fn main() {
    mut config := Config{
        prj_name: 'myprj'
        language: 'python'
        author: os.loginname()
    }

    mut args := set_arguments()
    args.parse(os.args) or {
        println(err)
        exit(8)
    }

    if args.is_option_set('a') {
        config.author = args.option_value('a')
	}

    config.prj_name = args.argument_value('name') or {
        config.prj_name = 'myprj'
        'myprj'
    }

	check_prerequisites(config) or {
		println(err)
		exit(8)
	}

	if args.is_option_set('l') {
        config.language = args.option_value('l')
    }

    match config.language {
        'python' {
            launch_python_project(config) or {
                println(err)
                exit(8)
            }
        }
        else {}
    }
}

fn check_prerequisites(cfg Config)? {
    println(cfg)
    if os.is_dir(cfg.prj_name) {
        if ! os.is_dir_empty(cfg.prj_name) {
            return error('$cfg.prj_name exists and is not empty. Quitting')
        }
    }
}

fn launch_python_project(cfg Config)? {
    print('Making sure you have Poetry installed')
    chk := os.execute('poetry -v')
    if chk.exit_code != 0 {
        return error('I had trouble finding Poetry. Please install it')
    }
    println('...ok\n')

    print('Creating new Python project folder $cfg.prj_name')
    mut cmd := 'poetry new --name $cfg.prj_name --src $cfg.prj_name'
    poetry := os.execute(cmd)
    if poetry.exit_code != 0 {
        return error('...Could not create the project')
    }
    println('...ok\n')

    print('Creating project documentation directory')
    os.chdir(cfg.prj_name) or {
        return error('...Could not switch to the new project directory. Quitting')
    }
    os.mkdir('docs') or {
        return error('...failed')
    }
    println('...ok\n')

    print('Adding a standard Python dependency checker to dev dependencies')
    cmd = 'poetry add --dev safety'
    safety := os.execute(cmd)
    if safety.exit_code != 0 {
        return error('...failed')
    }
    println('...ok\n')

    print('Installing the latest version of Pytest')
    cmd = 'poetry add --dev pytest@latest'
    latest := os.execute(cmd)
    if latest.exit_code != 0 {
        return error('...failed')
    }
    println('...ok\n')

    print('Adding Sphinx documentation generator to project $cfg.prj_name')
    cmd = 'poetry add --dev sphinx'
    sphinx := os.execute(cmd)
    if sphinx.exit_code != 0 {
        return error('...Could not add Sphinx to this project')
    }
    println('...ok\n')

    print('Configuring the Sphinx documentation system for this project')
    cmd = 'poetry run sphinx-quickstart --sep -p $cfg.prj_name ' +
        '-a \"$cfg.author\" -l \'en\' --release= --ext-autodoc ' +
        '--ext-intersphinx --ext-todo docs'
    setup := os.execute(cmd)
    if setup.exit_code != 0 {
        return error('...failed. Please review output and try again')
    }
    println('...ok\n')

    if ! os.is_file('docs/source/conf.py') {
        return error('Something went wrong. Cannot see Sphinx config file')
    }

    // Looks like the following is not needed for Linux
    println('Making changes to the Sphinx config file')
    print('Backing it up first')
    os.mv('docs/source/conf.py', 'docs/source/orig-conf.py') or {
        return error('...failed')
    }
    println('...ok\n')

    print('Output updated Sphinx config file')

    mut out_buffer := '# Updated by an automated script\n\n'
    out_buffer = out_buffer + 'import os\nimport sys\n' +
        "sys.path.insert(0, os.path.abspath('../../src/$cfg.prj_name'))\n\n"
    orig_config := os.read_lines('docs/source/orig-conf.py') or {
        return error('...fail1')
    }
    for this_line in orig_config {
        out_buffer = out_buffer + this_line + '\n'
    }
    os.write_file('docs/source/conf.py', out_buffer) or {
        return error('...failed')
    }
    println('...ok\n')

    // Install!
    print('Installing the new environment into a local sandbox')
    cmd = 'poetry install'
    install := os.execute(cmd)
    if install.exit_code != 0 {
        return error('...failed. Please review output and try again')
    }
    println('...ok\n')

    // Setting up a quick test
    if ! os.is_dir('TestResults') {
        print('Creating a TestResults folder')
        os.mkdir('TestResults') or {
            return error('...failed')
        }
        println('...ok\n')

        print('Writing a pytest config file')
        mut buffer := '# This file was autogenerated by the scaffolding script\n'
        buffer = buffer + '\n[pytest]\n'
        buffer = buffer + 'junit_family=xunit2\n'
        buffer = buffer + 'addopts = --junitxml=TestResults/report.xml\n'

        os.write_file('pytest.ini', buffer) or {
            return error('...failed')
        }
        println('...ok\n')

        print('Running pytest, just to make sure')
        cmd = 'poetry run pytest'
        trun := os.execute(cmd)
        if trun.exit_code != 0 {
            return error('...failed. Please review output and try again')
        }
        println('...ok\n')
    }

    println('Done!')
}

fn set_arguments() getopts.Cmd_line {
    mut ncl := getopts.new_cmd_line() or {
        println(err)
        exit(8)
    }
    ncl.add_argument('name', 'The name of the project to create') or {
        println(err)
        exit(8)
    }
    ncl.add_option('l', 'language', 'python', 'The language this is written in') or {
        println(err)
        exit(8)
    }
    ncl.add_option('a', 'author', 'v', 'Author') or {
        println(err)
        exit(8)
    }

    return ncl
}
